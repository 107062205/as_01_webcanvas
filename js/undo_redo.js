var PushArray = new Array();
var Step = -1;
document.body.onload = function () {
    PushState();
};

document.getElementById('Undo').addEventListener('click', function () {
    if (Step > 0) {
        Step--;

        var Pic = new Image();
        Pic.onload = function () {
            ctx.clearRect(0, 0, canvas.offsetWidth, canvas.offsetHeight);
            ctx.drawImage(Pic, 0, 0);
        }
        Pic.src = PushArray[Step];

    }
})
document.getElementById('Redo').addEventListener('click', function () {
    if (Step < PushArray.length - 1) {
        Step++;
        var Pic = new Image();
        Pic.src = PushArray[Step];
        Pic.onload = function () {
            ctx.clearRect(0, 0, canvas.offsetWidth, canvas.offsetHeight);
            ctx.drawImage(Pic, 0, 0);
        }
    }
})
function PushState() {
    Step++;
    if (Step < PushArray.length) {
        PushArray.length = Step;
    }
    PushArray.push(document.getElementById('Canvas').toDataURL());
}