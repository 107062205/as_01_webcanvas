canvas.addEventListener('mousedown', function (e) {

    e.preventDefault();
    e.stopPropagation();
    if (!inrange(canvas, e) || Mouse.SelectedMode < MODE.Square) {
        return;
    }

    Mouse.Block = document.getElementById('ShapePosition');
    Mouse.Block.style.cursor = "crosshair";
    Mouse.StartX = e.clientX;
    Mouse.StartY = e.clientY;
    Mouse.Block.style.left = Mouse.StartX + 'px';
    Mouse.Block.style.top = Mouse.StartY + 'px';
    Mouse.Block.style.border = Mouse.LineWidth + 'px solid ' + Mouse.Color;
    Mouse.Block.style.visibility = '';
    Mouse.Block.style.backgroundColor = '';

    if (Mouse.SelectedMode == MODE.Square) {
        Mouse.Block.className = 'Rectangle';
        Mouse.Block.style.backgroundColor = Mouse.Color;
    }
    else if (Mouse.SelectedMode == MODE.Circle) {
        Mouse.Block.className = 'Circle';
        Mouse.Block.style.backgroundColor = Mouse.Color;
    }
    else if (Mouse.SelectedMode == MODE.Triangle) {
        Mouse.Block.className = 'Triangle';
        Mouse.Block.style.borderTop = Mouse.Block.style.borderBottom = '';
    }
});
canvas.addEventListener('mousemove', function (e) {

    e.preventDefault();
    e.stopPropagation();
    if (inrange(canvas, e) == true) {
        if (Mouse.Block == null || Mouse.SelectedMode < MODE.Square) {
            return;
        }
        Mouse.CurX = e.clientX;
        Mouse.CurY = e.clientY;
        Mouse.Width = Math.abs(Mouse.CurX - Mouse.StartX);
        Mouse.Height = Math.abs(Mouse.CurY - Mouse.StartY);

        if (Mouse.SelectedMode == MODE.Square) {
            Mouse.Block.style.width = Mouse.Width + 'px';
            Mouse.Block.style.height = Mouse.Height + 'px';
            Mouse.Block.style.left = Math.min(Mouse.StartX, Mouse.CurX) + 'px';
            Mouse.Block.style.top = Math.min(Mouse.StartY, Mouse.CurY) + 'px';
        }
        else if (Mouse.SelectedMode == MODE.Circle) {
            Mouse.Width = Mouse.Height = Math.max(Mouse.Height, Mouse.Width);
            Mouse.Block.style.left = (Mouse.CurX + Mouse.StartX - Mouse.Width) / 2 + 'px';
            Mouse.Block.style.top = (Mouse.CurY + Mouse.StartY - Mouse.Height) / 2 + 'px';
            Mouse.Block.style.width = Mouse.Block.style.height = Mouse.Width + 'px';
        }
        else if (Mouse.SelectedMode == MODE.Triangle) {
            if (Mouse.CurY > Mouse.StartY) {
                Mouse.Block.style.borderTop = Mouse.Height + 'px solid ' + Mouse.Color;
                Mouse.Block.style.borderBottom = '';
            }
            else {
                Mouse.Block.style.borderBottom = Mouse.Height + 'px solid ' + Mouse.Color;
                Mouse.Block.style.borderTop = '';
            }
            Mouse.Block.style.left = Math.min(Mouse.StartX, Mouse.CurX) + 'px';
            Mouse.Block.style.top = Math.min(Mouse.StartY, Mouse.CurY) + 'px';
            Mouse.Block.style.borderLeft = (Mouse.Width / 2) + 'px solid transparent';
            Mouse.Block.style.borderRight = (Mouse.Width / 2) + 'px solid transparent';
        }

    }
    else {
        return;
    }
});
window.addEventListener('mouseup', function (e) {
    if (Mouse.SelectedMode < MODE.Square) {
        return;
    }
    e.preventDefault();
    e.stopPropagation();
    if (Mouse.Block == null || Mouse.SelectedMode < MODE.Square) {
        return;
    }
    Mouse.Block.style.height = Mouse.Block.style.width = '0px';
    Mouse.Block.style.visibility = 'hidden';
    Mouse.Block = null;
    if (Mouse.SelectedMode == MODE.Square) {
        var left = Math.min(Mouse.StartX, Mouse.CurX);
        var top = Math.min(Mouse.StartY, Mouse.CurY);
        var h = Mouse.Height;
        var w = Mouse.Width;
        ctx.beginPath();
        ctx.fillStyle = Mouse.Color;
        ctx.fillRect(left - canvas.offsetLeft, top - canvas.offsetTop, w, h);
    }
    else if (Mouse.SelectedMode == MODE.Circle) {
        var X = Math.abs(Mouse.StartX + Mouse.CurX) / 2 - canvas.offsetLeft;
        var Y = Math.abs(Mouse.StartY + Mouse.CurY) / 2 - canvas.offsetTop;
        var r = Mouse.Width;
        ctx.beginPath();
        ctx.fillStyle = Mouse.Color;
        ctx.arc(X, Y, r / 2, 0, 2 * Math.PI);
        ctx.fill();

    }
    else if (Mouse.SelectedMode == MODE.Triangle) {
        ctx.beginPath();
        var leftX = Mouse.StartX - canvas.offsetLeft;
        var leftY = Mouse.StartY - canvas.offsetTop;
        var rightX = Mouse.CurX - canvas.offsetLeft;
        var rightY = leftY;
        var topX = (leftX + rightX) / 2;
        var topY = Mouse.CurY - canvas.offsetTop;
        ctx.fillStyle = Mouse.Color;
        ctx.moveTo(leftX, leftY);
        ctx.lineTo(rightX, rightY);
        ctx.lineTo(topX, topY);
        ctx.lineTo(leftX, leftY);
        ctx.fill();
    }
    PushState();

});
function inrange(canvas, e) {
    if (canvas.offsetLeft <= e.pageX && e.pageX <= canvas.offsetLeft + canvas.offsetWidth && canvas.offsetTop <= e.pageY && e.pageY <= canvas.offsetTop + canvas.offsetHeight)
        return true;
    else
        return false;

}