const MODE = {
    Pen: 0,
    Eraser: 1,
    Text: 2,
    Square: 3,
    Triangle: 4,
    Circle: 5,
};

var Mouse = {
    Down: false,
    Color: "black", // pen Color, selectd by buttom
    LineWidth: 1, //pen lineWidth , selectd by buttom
    SelectedMode: MODE.Pen, //current mode
    ShapeFill: true,
    Font: 'Arial',
    CurX: 0,
    CurY: 0,
    StartX: 0,
    StartY: 0,
    Height: 0,
    Weight: 0,
    Block: null
};

var canvas = document.getElementById('Canvas');
var ctx = canvas.getContext('2d');
console.log('107062205 鍾宇騫 SS HW1');
window.onload = window.onresize = function () {
    canvas.width = window.innerWidth * 0.8;
    canvas.height = window.innerHeight * 0.8;
}

canvas.addEventListener('mousedown', function (e) {
    Mouse.Down = true;
    ctx.strokeStyle = Mouse.Color;
    ctx.lineWidth = Mouse.LineWidth;
    if (Mouse.SelectedMode == MODE.Pen || Mouse.SelectedMode == MODE.Eraser) {
        ctx.beginPath();
        ctx.moveTo(e.pageX - canvas.offsetLeft, e.pageY - canvas.offsetTop);
        Mouse.CurX = e.pageX - canvas.offsetLeft;
        Mouse.CurY = e.pageY - canvas.offsetTop;
    }
});

canvas.addEventListener('mousemove', function (e) {
    var W = Mouse.LineWidth;
    if (Mouse.Down == true) {
        if (Mouse.SelectedMode == MODE.Pen) {
            ctx.lineTo(e.pageX - canvas.offsetLeft, e.pageY - canvas.offsetTop);
            ctx.stroke();
        } else if (Mouse.SelectedMode == MODE.Eraser) {
            ctx.clearRect(e.pageX - canvas.offsetLeft - W * 4, e.pageY - canvas.offsetTop - W * 4, W * 8, W * 8);
        }
    }
});

canvas.addEventListener('mouseup', function (e) {
    Mouse.Down = false;
    PushState();
});

canvas.addEventListener('click', function (e) {
    if (Mouse.SelectedMode == MODE.Text) {
        Mouse.curX = e.pageX - canvas.offsetLeft;
        Mouse.CurY = e.pageY - canvas.offsetTop;

        var Position = document.getElementById('InputPosition');
        Position.style.position = 'absolute';
        Position.style.left = e.pageX + 'px';
        Position.style.top = e.pageY + 'px';
        Position.innerHTML = "<input type='text' id='TextInput'>";
    }
});

document.addEventListener('keydown', function (e) {
    if (e.keyCode == 13 && Mouse.SelectedMode == MODE.Text) {

        var FontSelect = document.getElementById('FontSelector');
        var FontFace = document.getElementById('FontFace');
        var text = document.getElementById('TextInput').value;
        var canvas = document.getElementById('Canvas');
        var div = document.getElementById('InputPosition');
        var ctx = canvas.getContext('2d');
        var FontSize = FontSelect.options[FontSelect.selectedIndex].value;
        var FontType = FontFace.options[FontFace.selectedIndex].value;
        ctx.fillStyle = Mouse.Color;
        ctx.font = FontSize + 'px ' + FontType;
        ctx.fillText(text, Mouse.curX, Mouse.CurY);
        div.innerHTML = "";
        PushState();
    }
});