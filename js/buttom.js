document.getElementById('Pen').addEventListener('click', function () {
    Mouse.SelectedMode = MODE.Pen;
    canvas.style.cursor = "url('src/brush.png'),auto";
});
document.getElementById('Eraser').addEventListener('click', function () {
    Mouse.SelectedMode = MODE.Eraser;
    canvas.style.cursor = "url('src/eraser.png'),auto";
});
document.getElementById('Square').addEventListener('click', function () {
    Mouse.SelectedMode = MODE.Square;
    canvas.style.cursor = "crosshair";
});

document.getElementById('Triangle').addEventListener('click', function () {
    Mouse.SelectedMode = MODE.Triangle;
    canvas.style.cursor = "crosshair";
});
document.getElementById('Circle').addEventListener('click', function () {
    Mouse.SelectedMode = MODE.Circle;
    canvas.style.cursor = "crosshair";
});
document.getElementById('Text').addEventListener('click', function () {
    Mouse.SelectedMode = MODE.Text;
    canvas.style.cursor = "text";
});
document.getElementById('Clean').addEventListener('click', function () {
    ctx.clearRect(0, 0, canvas.offsetWidth, canvas.offsetHeight);
    PushState();
});
document.getElementById('Bar').addEventListener('mouseup', function () {
    Mouse.LineWidth = document.getElementById('Bar').value;
})