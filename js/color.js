var ColorBlock = document.getElementById('ColorBlock');
var CtxBlock = ColorBlock.getContext('2d');
var width1 = ColorBlock.width;
var height1 = ColorBlock.height;
var ColorStrip = document.getElementById('ColorStrip');
var CtxStrip = ColorStrip.getContext('2d');
var width2 = ColorStrip.width;
var height2 = ColorStrip.height;
var ColorLabel = document.getElementById('ColorLabel');
var x = 0;
var y = 0;
var drag = false;
var rgbaColor = 'rgba(0,0,0,1)';

CtxBlock.rect(0, 0, width1, height1);
FillColor();
CtxStrip.rect(0, 0, width2, height2);
var grd1 = CtxStrip.createLinearGradient(0, 0, 0, height1);
grd1.addColorStop(0, 'rgba(255, 0, 0, 1)');
grd1.addColorStop(0.20, 'rgba(255, 255, 0, 1)');
grd1.addColorStop(0.40, 'rgba(0, 255, 0, 1)');
grd1.addColorStop(0.55, 'rgba(0, 255, 255, 1)');
grd1.addColorStop(0.70, 'rgba(0, 0, 255, 1)');
grd1.addColorStop(0.85, 'rgba(255, 0, 255, 1)');
grd1.addColorStop(1, 'rgba(255, 0, 0, 1)');
CtxStrip.fillStyle = grd1;
CtxStrip.fill();

ColorStrip.addEventListener("click", function (e) {
    x = e.offsetX;
    y = e.offsetY;
    var imageData = CtxStrip.getImageData(x, y, 1, 1).data;
    rgbaColor = 'rgba(' + imageData[0] + ',' + imageData[1] + ',' + imageData[2] + ',1)';
    FillColor();
});

ColorBlock.addEventListener("mousedown", function (e) {
    drag = true;
    ChangeColor(e);
});

ColorBlock.addEventListener("mousemove", function (e) {
    if (drag == true) {
        ChangeColor(e);
    }
});
ColorBlock.addEventListener("mouseup", function (e) {
    drag = false;
});


function FillColor() {
    CtxBlock.fillStyle = rgbaColor;
    CtxBlock.fillRect(0, 0, width1, height1);

    var grdWhite = CtxStrip.createLinearGradient(0, 0, width1, 0);
    grdWhite.addColorStop(0, 'rgba(255,255,255,1)');
    grdWhite.addColorStop(1, 'rgba(255,255,255,0)');
    CtxBlock.fillStyle = grdWhite;
    CtxBlock.fillRect(0, 0, width1, height1);

    var grdBlack = CtxStrip.createLinearGradient(0, 0, 0, height1);
    grdBlack.addColorStop(0, 'rgba(0,0,0,0)');
    grdBlack.addColorStop(1, 'rgba(0,0,0,1)');
    CtxBlock.fillStyle = grdBlack;
    CtxBlock.fillRect(0, 0, width1, height1);
}

function ChangeColor(e) {
    x = e.offsetX;
    y = e.offsetY;
    var imageData = CtxBlock.getImageData(x, y, 1, 1).data;
    rgbaColor = 'rgba(' + imageData[0] + ',' + imageData[1] + ',' + imageData[2] + ',1)';
    ColorLabel.style.backgroundColor = rgbaColor;
    Mouse.Color = rgbaColor;
}
