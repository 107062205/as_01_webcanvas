document.getElementById('download').addEventListener('click', function () {
    var download = document.getElementById("download");
    var image = document.getElementById("Canvas").toDataURL("image/png")
        .replace("image/png", "image/octet-stream");
    download.setAttribute("href", image);
});
document.getElementById('upload').addEventListener('change', function () {
    var img = new Image();
    img.onload = function () {
        canvas.width = this.width;
        canvas.height = this.height;
        ctx.drawImage(this, 0, 0);
    }
    img.src = URL.createObjectURL(this.files[0]);
    PushState();
});