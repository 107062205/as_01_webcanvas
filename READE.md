# Software Studio 2020 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Name of widgets                                  | 1~5%     | N         |


---

### How to use 

#### Brush: 
Click the Brush buttom,then drag to draw. 
![](https://i.imgur.com/ED5syOo.png)

####  Eraser: 
 Clik the Eraser buttom,then drag to erase
![](https://i.imgur.com/DEkupc1.png) 

#### Brush/Eraser Size
drag the bar to choose the size you want
![](https://i.imgur.com/CbQ2CYA.jpg)

#### Color Selecter: 
choose form right two area to get the color you want.
And the left top block will show the seleced color
![](https://i.imgur.com/80Au9m5.jpg)


#### Text Input
    
Click Text Buttom then click the place you want to add word.
After entering the word , press **ENTER** to add word on canvas
![](https://i.imgur.com/GgInp9U.jpg)

Also, you can pick the font size and font face from the scroll below

![](https://i.imgur.com/gzaslbd.png)![](https://i.imgur.com/EszXJ5K.png)

#### Cursor icon
For Brush ,Eraser ,Color Selecter , Text Input ,and Shape,
they all have special cirsor icon when you select each mode.

#### Refresh Buttom
click the clear buttom can clear all the canvas
![](https://i.imgur.com/mbPO4U2.jpg)

#### 3 different Shape
Choose the Shape buttom to select the shape you want,
also you can choose color from color selecter.
Rectangle: generate a rectangle form mouse down position to mouse up position
Triangle : if the start position uppe than end position, it will generate a Inverted triangle.
Circle   : raius= (distance from start position to end position)/2, 
center =(start position + end position)/2

![](https://i.imgur.com/Ru79R2l.jpg)

![](https://i.imgur.com/QdC6jxs.jpg)

#### Undo Redo Buttom
click the buttom below can undo or redo drawing
left : undo 
right:redo
![](https://i.imgur.com/IYtS7K6.jpg)

#### Image Tool
click "選擇檔案" to upload a image to the canvas background
![](https://i.imgur.com/Aizjd2S.jpg)

#### Download
click the icon below to download the canvas.
![](https://i.imgur.com/OLSCxZI.jpg)


### Function description
I have **Mouse** to store the mouse detail for the function.
And **MODE** is for Select mode
```javascript=
const MODE = {
    Pen: 0,
    Eraser: 1,
    Text: 2,
    Square: 3,
    Triangle: 4,
    Circle: 5,
};

var Mouse = {
    Down: false,
    Color: "black", // pen Color, selectd by buttom
    LineWidth: 1, //pen lineWidth , selectd by buttom
    SelectedMode: MODE.Pen, //current mode
    ShapeFill: true,
    Font: 'Arial',
    CurX: 0,
    CurY: 0,
    StartX: 0,
    StartY: 0,
    Height: 0,
    Weight: 0,
    Block: null
};
```



#### Brush and Eraser
use these 3 function to do painting and erasing, mode is controled by "**Mouse.SelecetedMode**"
mouse down : enable drawing control var "**Mouse.Down**" and set the start position
``` javascript=
canvas.addEventListener('mousedown', function (e) {
Mouse.Down = true;
ctx.strokeStyle = Mouse.Color;
ctx.lineWidth = Mouse.LineWidth;
if (Mouse.SelectedMode == MODE.Pen || Mouse.SelectedMode == MODE.Eraser)     {
    ctx.beginPath();
    ctx.moveTo(e.pageX - canvas.offsetLeft, e.pageY - canvas.offsetTop);
    Mouse.CurX = e.pageX - canvas.offsetLeft;
    Mouse.CurY = e.pageY - canvas.offsetTop;
}
});
```


mouse move : use linto() to draw a line , implement the painting  
``` javascript=
canvas.addEventListener('mousemove', function (e) {
    var W = Mouse.LineWidth;
    if (Mouse.Down == true) {
        if (Mouse.SelectedMode == MODE.Pen) {
            ctx.lineTo(e.pageX - canvas.offsetLeft, e.pageY - canvas.offsetTop);
            ctx.stroke();
        } else if (Mouse.SelectedMode == MODE.Eraser) {
            ctx.clearRect(e.pageX - canvas.offsetLeft - W * 4, e.pageY - canvas.offsetTop - W * 4, W * 8, W * 8);
        }
    }
});
```

mouseup: disable drawing, and push current state into the array for undo function. 

``` javascript=
canvas.addEventListener('mouseup', function (e) {
    Mouse.Down = false;
    PushState();
});
```

#### Color Selecter
There is 3 part of the Color Selecter.
1. Lebel
    ![](https://i.imgur.com/6M86ZXT.jpg)
    Show the color selected,which is a lebel in HTML file.
    after select color, I use ChangeColor to update the label's color. 
``` javascript=
function ChangeColor(e) {
    x = e.offsetX;
    y = e.offsetY;
    var imageData = CtxBlock.getImageData(x, y, 1, 1).data;
    rgbaColor = 'rgba(' + imageData[0] + ',' + imageData[1] + ',' + imageData[2] + ',1)';
    ColorLabel.style.backgroundColor = rgbaColor;
    Mouse.Color = rgbaColor;
}
```
2. Strip
![](https://i.imgur.com/R8y06B1.jpg)

    Show the color type to select, which is implement by drawing gradient and click event.
    FillColor( ) is for Block to update color gradient.
    
``` javascript=
var grd1 = CtxStrip.createLinearGradient(0, 0, 0, height1);
grd1.addColorStop(0, 'rgba(255, 0, 0, 1)');
grd1.addColorStop(0.20, 'rgba(255, 255, 0, 1)');
grd1.addColorStop(0.40, 'rgba(0, 255, 0, 1)');
grd1.addColorStop(0.55, 'rgba(0, 255, 255, 1)');
grd1.addColorStop(0.70, 'rgba(0, 0, 255, 1)');
grd1.addColorStop(0.85, 'rgba(255, 0, 255, 1)');
grd1.addColorStop(1, 'rgba(255, 0, 0, 1)');
CtxStrip.fillStyle = grd1;
CtxStrip.fill();

ColorStrip.addEventListener("click", function (e) {
    x = e.offsetX;
    y = e.offsetY;
    var imageData = CtxStrip.getImageData(x, y, 1, 1).data;
    rgbaColor = 'rgba(' + imageData[0] + ',' + imageData[1] + ',' + imageData[2] + ',1)';
    FillColor();
});
```
3. Block
    ![](https://i.imgur.com/OrLPRIm.jpg)
    Color is selected at here by clicking anywhere in Block.
    
``` javascript=
ColorBlock.addEventListener("mousedown", function (e) {
    drag = true;
    ChangeColor(e);
});

ColorBlock.addEventListener("mousemove", function (e) {
    if (drag == true) {
        ChangeColor(e);
    }
});
ColorBlock.addEventListener("mouseup", function (e) {
    drag = false;
});
```

#### Text Input
This function is based on a div, I changed the innerHTML when I need to use it just like the code below.
InputPosition is a div a put in HTML file, after using, it will be hide by remove the innerHTML.
``` javascript=
canvas.addEventListener('click', function (e) {
    if (Mouse.SelectedMode == MODE.Text) {
        Mouse.curX = e.pageX - canvas.offsetLeft;
        Mouse.CurY = e.pageY - canvas.offsetTop;

        var Position = document.getElementById('InputPosition');
        Position.style.position = 'absolute';
        Position.style.left = e.pageX + 'px';
        Position.style.top = e.pageY + 'px';
        Position.innerHTML = "<input type='text' id='TextInput'>";
    }
});

document.addEventListener('keydown', function (e) {
    if (e.keyCode == 13 && Mouse.SelectedMode == MODE.Text) {

        var FontSelect = document.getElementById('FontSelector');
        var FontFace = document.getElementById('FontFace');
        var text = document.getElementById('TextInput').value;
        var canvas = document.getElementById('Canvas');
        var div = document.getElementById('InputPosition');
        var ctx = canvas.getContext('2d');
        var FontSize = FontSelect.options[FontSelect.selectedIndex].value;
        var FontType = FontFace.options[FontFace.selectedIndex].value;
        ctx.fillStyle = Mouse.Color;
        ctx.font = FontSize + 'px ' + FontType;
        ctx.fillText(text, Mouse.curX, Mouse.CurY);
        div.innerHTML = "";
        PushState();
    }
});
```
and the FontSize and FontFace is implemented 2 selecter and by those code below in "**keydown**" event.
``` HTML=
<select id="FontSelector" name="FontSize">
      <option value="16">16</option>
      <option value="20">20</option>
      <option value="24" selected="selected">24</option>
      <option value="30">30</option>
      <option value="42">42</option>
      <option value="56">56</option>
      <option value="70">70</option>
    </select>

    <select id="FontFace" name="FontFace">
      <option value="微軟正黑體">微軟正黑體</option>
      <option value="新細明體">新細明體</option>
      <option value="標楷體">標楷體</option>
      <option value="Arial" selected="selected">Arial</option>
      <option value="serif">serif</option>
      <option value="cursive">cursive</option>
      <option value="fantasy">fantasy</option>
    </select> <br>
```
``` javascript=
var FontSize = FontSelect.options[FontSelect.selectedIndex].value;
var FontType = FontFace.options[FontFace.selectedIndex].value;
ctx.font = FontSize + 'px ' + FontType;
```

#### Cursor Icon
it is implemented by buttom click event when choosing a mode.
``` javascript=
document.getElementById('Pen').addEventListener('click', function () {
    Mouse.SelectedMode = MODE.Pen;
    canvas.style.cursor = "url('src/brush.png'),auto";
});
document.getElementById('Eraser').addEventListener('click', function () {
    Mouse.SelectedMode = MODE.Eraser;
    canvas.style.cursor = "url('src/eraser.png'),auto";
});
document.getElementById('Square').addEventListener('click', function () {
    Mouse.SelectedMode = MODE.Square;
    canvas.style.cursor = "crosshair";
});

document.getElementById('Triangle').addEventListener('click', function () {
    Mouse.SelectedMode = MODE.Triangle;
    canvas.style.cursor = "crosshair";
});
document.getElementById('Circle').addEventListener('click', function () {
    Mouse.SelectedMode = MODE.Circle;
    canvas.style.cursor = "crosshair";
});
document.getElementById('Text').addEventListener('click', function () {
    Mouse.SelectedMode = MODE.Text;
    canvas.style.cursor = "text";
});
```
#### Refersh Buttom
when click clear buttom, this event will use clearRect( ) to clean canvas completly.
``` javascript=

document.getElementById('Clean').addEventListener('click', function () {
    ctx.clearRect(0, 0, canvas.offsetWidth, canvas.offsetHeight);
    PushState();
});
```

#### 3 shape
base on 3 mouse event, then I use a div to simulate the draging scene, after mouseup, I hide the div and draw the shape by the recorded details.
1. mousedown
when mouse down, set the initial state of mouse and shape detail.
``` javascript=
canvas.addEventListener('mousedown', function (e) {

    e.preventDefault();
    e.stopPropagation();
    if (!inrange(canvas, e) || Mouse.SelectedMode < MODE.Square) {
        return;
    }

    Mouse.Block = document.getElementById('ShapePosition');
    Mouse.Block.style.cursor = "crosshair";
    Mouse.StartX = e.clientX;
    Mouse.StartY = e.clientY;
    Mouse.Block.style.left = Mouse.StartX + 'px';
    Mouse.Block.style.top = Mouse.StartY + 'px';
    Mouse.Block.style.border = Mouse.LineWidth + 'px solid ' + Mouse.Color;
    Mouse.Block.style.visibility = '';
    Mouse.Block.style.backgroundColor = '';

    if (Mouse.SelectedMode == MODE.Square) {
        Mouse.Block.className = 'Rectangle';
        Mouse.Block.style.backgroundColor = Mouse.Color;
    }
    else if (Mouse.SelectedMode == MODE.Circle) {
        Mouse.Block.className = 'Circle';
        Mouse.Block.style.backgroundColor = Mouse.Color;
    }
    else if (Mouse.SelectedMode == MODE.Triangle) {
        Mouse.Block.className = 'Triangle';
        Mouse.Block.style.borderTop = Mouse.Block.style.borderBottom = '';
    }
});
```
2. mousemove
when mouse moving, update Mouse's and div's detail.
``` javascript=
canvas.addEventListener('mousemove', function (e) {
    e.preventDefault();
    e.stopPropagation();
    if (inrange(canvas, e) == true) {
        if (Mouse.Block == null || Mouse.SelectedMode < MODE.Square) {
            return;
        }
        Mouse.CurX = e.clientX;
        Mouse.CurY = e.clientY;
        Mouse.Width = Math.abs(Mouse.CurX - Mouse.StartX);
        Mouse.Height = Math.abs(Mouse.CurY - Mouse.StartY);

        if (Mouse.SelectedMode == MODE.Square) {
            Mouse.Block.style.width = Mouse.Width + 'px';
            Mouse.Block.style.height = Mouse.Height + 'px';
            Mouse.Block.style.left = Math.min(Mouse.StartX, Mouse.CurX) + 'px';
            Mouse.Block.style.top = Math.min(Mouse.StartY, Mouse.CurY) + 'px';
        }
        else if (Mouse.SelectedMode == MODE.Circle) {
            Mouse.Width = Mouse.Height = Math.max(Mouse.Height, Mouse.Width);
            Mouse.Block.style.left = (Mouse.CurX + Mouse.StartX - Mouse.Width) / 2 + 'px';
            Mouse.Block.style.top = (Mouse.CurY + Mouse.StartY - Mouse.Height) / 2 + 'px';
            Mouse.Block.style.width = Mouse.Block.style.height = Mouse.Width + 'px';
        }
        else if (Mouse.SelectedMode == MODE.Triangle) {
            if (Mouse.CurY > Mouse.StartY) {
                Mouse.Block.style.borderTop = Mouse.Height + 'px solid ' + Mouse.Color;
                Mouse.Block.style.borderBottom = '';
            }
            else {
                Mouse.Block.style.borderBottom = Mouse.Height + 'px solid ' + Mouse.Color;
                Mouse.Block.style.borderTop = '';
            }
            Mouse.Block.style.left = Math.min(Mouse.StartX, Mouse.CurX) + 'px';
            Mouse.Block.style.top = Math.min(Mouse.StartY, Mouse.CurY) + 'px';
            Mouse.Block.style.borderLeft = (Mouse.Width / 2) + 'px solid transparent';
            Mouse.Block.style.borderRight = (Mouse.Width / 2) + 'px solid transparent';
        }

    }
    else {
        return;
    }
});
```

3. mouseup
when mouse up, hide the div used to simulate draging scene and draw the shape.
``` javascript=
window.addEventListener('mouseup', function (e) {
    if (Mouse.SelectedMode < MODE.Square) {
        return;
    }
    e.preventDefault();
    e.stopPropagation();
    if (Mouse.Block == null || Mouse.SelectedMode < MODE.Square) {
        return;
    }
    Mouse.Block.style.height = Mouse.Block.style.width = '0px';
    Mouse.Block.style.visibility = 'hidden';
    Mouse.Block = null;
    if (Mouse.SelectedMode == MODE.Square) {
        var left = Math.min(Mouse.StartX, Mouse.CurX);
        var top = Math.min(Mouse.StartY, Mouse.CurY);
        var h = Mouse.Height;
        var w = Mouse.Width;
        ctx.beginPath();
        ctx.fillStyle = Mouse.Color;
        ctx.fillRect(left - canvas.offsetLeft, top - canvas.offsetTop, w, h);
    }
    else if (Mouse.SelectedMode == MODE.Circle) {
        var X = Math.abs(Mouse.StartX + Mouse.CurX) / 2 - canvas.offsetLeft;
        var Y = Math.abs(Mouse.StartY + Mouse.CurY) / 2 - canvas.offsetTop;
        var r = Mouse.Width;
        ctx.beginPath();
        ctx.fillStyle = Mouse.Color;
        ctx.arc(X, Y, r / 2, 0, 2 * Math.PI);
        ctx.fill();

    }
    else if (Mouse.SelectedMode == MODE.Triangle) {
        ctx.beginPath();
        var leftX = Mouse.StartX - canvas.offsetLeft;
        var leftY = Mouse.StartY - canvas.offsetTop;
        var rightX = Mouse.CurX - canvas.offsetLeft;
        var rightY = leftY;
        var topX = (leftX + rightX) / 2;
        var topY = Mouse.CurY - canvas.offsetTop;
        ctx.fillStyle = Mouse.Color;
        ctx.moveTo(leftX, leftY);
        ctx.lineTo(rightX, rightY);
        ctx.lineTo(topX, topY);
        ctx.lineTo(leftX, leftY);
        ctx.fill();
    }
    PushState();

});
```

#### Undo / Redo
I use **PushState()** to push current state aftering every drawing just like the code I have shown.
``` javascript=
var PushArray = new Array();
var Step = -1;
function PushState() {
    Step++;
    if (Step < PushArray.length) {
        PushArray.length = Step;
    }
    PushArray.push(document.getElementById('Canvas').toDataURL());
}
```

undo and redo are done by moving the Step varible and push state.
``` javascript=
document.getElementById('Undo').addEventListener('click', function () {
    if (Step > 0) {
        Step--;

        var Pic = new Image();
        Pic.onload = function () {
            ctx.clearRect(0, 0, canvas.offsetWidth, canvas.offsetHeight);
            ctx.drawImage(Pic, 0, 0);
        }
        Pic.src = PushArray[Step];

    }
})
document.getElementById('Redo').addEventListener('click', function () {
    if (Step < PushArray.length - 1) {
        Step++;
        var Pic = new Image();
        Pic.src = PushArray[Step];
        Pic.onload = function () {
            ctx.clearRect(0, 0, canvas.offsetWidth, canvas.offsetHeight);
            ctx.drawImage(Pic, 0, 0);
        }
    }
})
```

#### Upload
when upload a image, I set canvas width and height as image's, then draw image on canvas, also I push the State into array , make the Redo Buttom works on this operation.
``` javascript=
document.getElementById('upload').addEventListener('change', function () {
    var img = new Image();
    img.onload = function () {
        canvas.width = this.width;
        canvas.height = this.height;
        ctx.drawImage(this, 0, 0);
    }
    img.src = URL.createObjectURL(this.files[0]);
    PushState();
});
```

#### download
I make download buttom be a link of current state's Image, so when you click a download buttom, just like click a image download link.
``` javascript=
document.getElementById('download').addEventListener('click', function () {
    var download = document.getElementById("download");
    var image = document.getElementById("Canvas").toDataURL("image/png")
        .replace("image/png", "image/octet-stream");
    download.setAttribute("href", image);
});
```
### Gitlab page link
[https://107062205.gitlab.io/as_01_webcanvas](https://107062205.gitlab.io/as_01_webcanvas/)
 

### Others (Optional)

    Anythinh you want to say to TAs.

<style>
table th{
    width: 100%;
}
</style>